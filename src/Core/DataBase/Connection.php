<?php

declare(strict_types=1);

namespace Coral\Core\DataBase;

use Coral\CoralException;
use Exception;
use Swoole\Database\PDOConfig;
use Swoole\Database\PDOPool;
use Throwable;

class Connection
{
    private static $instance;
    /**
     * @var PDOPool
     */
    protected PDOPool $pools;
    protected array   $config
        = [
            'host'       => 'localhost',
            'port'       => 3306,
            'database'   => 'test',
            'username'   => 'root',
            'password'   => 'root',
            'charset'    => 'utf8mb4',
            'unixSocket' => null,
            'options'    => [],
            'size'       => 64,
        ];

    /**
     * @throws CoralException
     */
    private function __construct(array $config)
    {
        if (empty($this->pools)) {
            $this->config = array_replace_recursive($this->config, $config);
            try {
                $this->pools = new PDOPool(
                    (new PDOConfig())
                        ->withHost($this->config['host'])
                        ->withPort($this->config['port'])
                        ->withUnixSocket($this->config['unixSocket'])
                        ->withDbName($this->config['database'])
                        ->withCharset($this->config['charset'])
                        ->withUsername($this->config['username'])
                        ->withPassword($this->config['password'])
                        ->withOptions($this->config['options']),
                    $this->config['size']
                );
            } catch (Throwable $th) {
                throw new CoralException($th->getMessage(), CoralException::EasyPdo_ERROR_CODE);
            }
        }
    }

    /**
     * @throws CoralException
     */
    public static function getInstance($config = null, $poolName = 'default')
    {
        try {
            if (empty(self::$instance[$poolName])) {
                if (empty($config)) {
                    throw new Exception('pdo config empty');
                }
                if (empty($config['size'])) {
                    throw new Exception('the size of database connection pools cannot be empty');
                }
                self::$instance[$poolName] = new static($config);
            }

            return self::$instance[$poolName];
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::EasyPdo_ERROR_CODE);
        }
    }

    /**
     * @throws CoralException
     */
    public function getConnection()
    {
        try {
            $connection = $this->pools->get();
            if (empty($connection)) {
                throw new Exception('pool is empty');
            }

            return $connection;
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::EasyPdo_ERROR_CODE);
        }
    }

    /**
     * @throws CoralException
     */
    public function close($connection = null)
    {
        try {
            $this->pools->put($connection);
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::EasyPdo_ERROR_CODE);
        }
    }
}