<?php

declare(strict_types=1);

namespace Coral\Core\DataBase;

class Raw
{
    public array  $map;
    public string $value;
}
