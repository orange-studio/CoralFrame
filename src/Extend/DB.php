<?php

namespace Coral\Extend;

use Coral\CoralException;
use Coral\Core\DataBase\BasePdo;
use Coral\Tool\ConfTool;
use Throwable;

class DB extends BasePdo
{
    /**
     * @throws CoralException
     */
    public function __construct(string $configName = 'default')
    {
        try {
            if (empty($config)) {
                if ($_ENV['ENV'] == 'PRODUCE') {
                    $config = ConfTool::GetConfig('DataBase.produce.' . $configName);
                } else {
                    $config = ConfTool::GetConfig('DataBase.develop.' . $configName);
                }
            }
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::EasyPdo_ERROR_CODE);
        }
        parent::__construct($config);
    }
}