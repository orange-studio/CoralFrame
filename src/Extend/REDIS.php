<?php

namespace Coral\Extend;

use Coral\CoralException;
use Coral\Core\Redis\BaseRedis;
use Coral\Tool\ConfTool;
use Exception;
use Throwable;

/**
 * Redis 组件
 */
class REDIS extends BaseRedis
{
    /**
     * @throws Exception
     */
    public function __construct(string $configName = 'default')
    {
        try {
            if ($_ENV['ENV'] == 'PRODUCE') {
                $config = ConfTool::GetConfig('Redis.produce.' . $configName);
            } else {
                $config = ConfTool::GetConfig('Redis.develop.' . $configName);
            }
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), CoralException::EasyRedis_ERROR_CODE);
        }
        parent::__construct($config);
    }

    public function getValue(string $key): ?string
    {
        return $this->get($key);
    }

    public function setValue(string $key, string $value, int $options): bool
    {
        return $this->set($key, $value, ['ex' => $options]);
    }

    public function delValue(string $key): bool
    {
        return $this->del($key);
    }

    /**
     * @throws CoralException
     * @throws Exception
     */
    public function busLock($key)
    {
        $res = $this->lock($key);
        if ($res === null) {
            throw new CoralException('操作太快,请稍候...');
        }
        return $res;
    }

    /**
     * @throws CoralException
     */
    public function busUnlock($key, $randNum)
    {
        return $this->unlock($key, $randNum);
    }
}