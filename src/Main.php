<?php

namespace Coral;

use Coral\Core\Http\HttpServer;
use Coral\Tool\ConfTool;
use Coral\Tool\UtilTool;
use EasyTask\Task;

class Main
{

    public const HTTP_SERVER   = 1;
    public const CORAL_VERSION = '2.0.3';
    private string $command;
    private bool   $daemonize;
    private Task   $task;
    private int    $mode;
    private array  $taskList;
    private        $HttpCallBack;

    public function __construct($argc, $argv)
    {
        // 确保至少有一个命令传入
        if ($argc <= 1) {
            $commandDes = "install      #安装框架\n";
            $commandDes .= "start       #启动服务\n";
            $commandDes .= "stop        #停止服务\n";
            $commandDes .= "restart     #重启服务\n";
            exit($commandDes);
        }
        // 加载入口文件内容
        $MainFile = CORAL_ROOT . DIRECTORY_SEPARATOR . 'main.php';
        if (!is_file($MainFile)) {
            exit('main.php 文件不存在');
        }

        include_once $MainFile;
        $this->command   = $argv[1];          // 获取命令行传入的命令
        $this->daemonize = $argv[2] ?? false; // 获取命令行传入的命令

        $this->task = new Task();
        // 是否守护进程启动
        $this->task->setDaemon($this->daemonize);
        // 设置系统时区
        $this->task->setTimeZone('Asia/Shanghai');
        // 设置服务名称
        $this->task->setPrefix('CoralServer');
        // 自动重启
        $this->task->setAutoRecover(true);
    }

    public function Run()
    {
        switch ($this->command) {
            case 'start':
                $this->ConsoleInfo();
                $this->task->start();
                break;
            case 'stop':
                $this->task->stop();
                break;
            case 'restart':
                $this->task->stop();
                $this->task->start();
                break;
            case 'status':
                $this->task->status();
                break;
            case 'reload':
                $this->Reload();
                break;
            default:
                // 如果命令不被识别，抛出一个异常
                exit("未识别的命令: {$this->command}");
        }
    }

    public function SetMode($mode)
    {
        $this->mode = $mode;
    }

    public function SetHttpCallBack(callable $HttpCallBack)
    {
        $this->HttpCallBack = $HttpCallBack;
    }

    public function Server(): Main
    {
        $mainFile = CORAL_ROOT . DIRECTORY_SEPARATOR . 'main.php';
        if (!is_file($mainFile)) {
            exit('主入口文件  main.php 不存在');
        }
        include_once $mainFile;
        if (empty($this->mode) || empty($this->HttpCallBack)) {
            exit('请设置mode和HttpCallBack');
        }
        $this->task->addFunc(function () {
            switch ($this->mode) {
                case self::HTTP_SERVER:
                    call_user_func($this->HttpCallBack, new HttpServer());
                    break;
                default:
                    exit('Mode not supported');
            }
        }, 'HttpServer', 0);
        return $this;
    }

    public function Tasks(array $tasks)
    {
        if (!empty($tasks)) {
            foreach ($tasks as $task) {
                if (!method_exists($task[0], $task[1])) {
                    exit("{$task[0]}::{$task[1]} is not exist");
                }
                $this->task->addClass($task[0], $task[1], $task[2], $task[3]);
            }
        }
        return $this;
    }

    /**
     * @throws CoralException
     */
    private function Reload()
    {
        $pidFile = UtilTool::NormalizedPath(CORAL_ROOT . DIRECTORY_SEPARATOR . ConfTool::GetConfig('request.pid_file'));
        // 确保 PID 文件存在
        if (!file_exists($pidFile)) {
            exit("PID file does not exist: {$pidFile}\n");
        }

        // 读取 PID
        $pid = file_get_contents($pidFile);
        if (!$pid) {
            exit("Failed to read PID file\n");
        }

        $pid = intval($pid);

        // 检查进程是否存在
        if (!posix_kill($pid, 0)) {
            exit("Process with PID {$pid} does not exist.\n");
        }

        // 发送 SIGUSR1 信号以触发 Swoole 服务重载
        if (posix_kill($pid, SIGUSR1)) {
            exit("coral server reload success\n");
        } else {
            exit("coral server reload fail\n");
        }
    }

    private function ConsoleInfo()
    {
        $info[] = '██████╗ ██████╗ ██████╗  █████╗ ██╗' . PHP_EOL;
        $info[] = '██╔════╝██╔═══██╗██╔══██╗██╔══██╗██║' . PHP_EOL;
        $info[] = '██╔════╝██╔═══██╗██╔══██╗██╔══██╗██║' . PHP_EOL;
        $info[] = '██║     ██║   ██║██████╔╝███████║██║' . PHP_EOL;
        $info[] = '██║     ██║   ██║██╔══██╗██╔══██║██║' . PHP_EOL;
        $info[] = '╚██████╗╚██████╔╝██║  ██║██║  ██║███████╗' . PHP_EOL;
        $info[] = '╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝' . PHP_EOL;
        vprintf("\033[31m %s%s%s%s%s%s%s \033[0m\n", $info);
        vprintf("\033[31m%-20s%20s\033[0m\n", ['LISTEN_ADDR', '0.0.0.0']);
        vprintf("\033[31m%-20s%20s\033[0m\n", ['LISTEN_PORT', ConfTool::GetConfig('server.port')]);
        vprintf("\033[31m%-20s%20s\033[0m\n", ['RUN_MODE', $_ENV['ENV']]);
        vprintf("\033[31m%-20s%20s\033[0m\n", ['PHP_VERSION', PHP_VERSION]);
        vprintf("\033[31m%-20s%20s\033[0m\n", ['SWOOLE_VERSION', SWOOLE_VERSION]);
        vprintf("\033[31m%-20s%20s\033[0m\n", ['CORAL_VERSION', self::CORAL_VERSION]);
        vprintf("\033[31m%-20s%20s\033[0m\n", ['REDIS_VERSION', phpversion('redis')]);
        vprintf("\033[31m%-20s%20s\033[0m\n", ['XLSWRITER_VERSION', phpversion('xlswriter')]);
    }


}