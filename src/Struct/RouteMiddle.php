<?php

namespace Coral\Struct;

class RouteMiddle
{
    public string $class            = '';
    public string $action           = '';
}