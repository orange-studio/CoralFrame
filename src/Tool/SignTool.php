<?php

namespace Coral\Tool;

class SignTool
{
    /**
     * 递归遍历数组拼接字符串
     *
     * @param array $data
     * @param string $key
     * @param bool $returnStr
     * @return string
     */
    public static function signStr(array $data, string $key = '', bool $returnStr = false): string
    {
        $string = '';
        ksort($data);
        foreach ($data as $k => $v) {
            if (is_array($v)) {
                $string .= static::signStr($v);
                continue;
            }
            if (($v || $v === 0 || $v === '0') && $k !== 'sign') {
                $string .= "$k=$v&";
            }
        }
        if (empty($key)) {
            return $string;
        }
        $signStr = $string . 'key=' . $key;
        if ($returnStr) {
            return $signStr;
        }
        return strtoupper(md5($signStr));
    }
}