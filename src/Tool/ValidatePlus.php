<?php

namespace Coral\Tool;

use Exception;
use Inhere\Validate\Validation;

/**
 * 参数校验组件
 */
class ValidatePlus
{

    /**
     * @Description 校验手机号
     *
     * @param $params
     * @return bool
     */
    public static function PhoneNumberFilter($params): bool
    {
        return !(preg_match_all('/^1\d{10}$/', $params) === 0);
    }

    /**
     * @Description 校验浮点型数据
     *
     * @param     $params
     * @param int $num
     * @return bool
     */
    public static function IsDecimalFilter($params, int $num = 2): bool
    {
        return !(preg_match_all('/^[0-9](\d+)?(\.\d{1,' . $num . '})?$/', $params) === 0);
    }

    /**
     * @Description 校验多个ID串联字符串
     *
     * @param     $params
     * @return bool
     */
    public static function IsIdStringFilter($params): bool
    {
        if (is_numeric($params) && $params <= 0) {
            return false;
        }
        return !(preg_match_all('/(^|,)\s*0\s*(,|$)/', $params) === 0);
    }

    /**
     * @Description 强密码校验(必须包含大小写字母和数字的组合，可以使用特殊字符，长度在8-16之间)
     *
     * @param     $params
     * @return bool
     */
    public static function PasswordFilter($params): bool
    {
        return !(preg_match_all('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,16}$/', $params) === 0);
    }

    /**
     * @param array $data
     * @param array $rule
     * @param array $message
     * @param int $Code
     * @return void
     * @throws Exception
     */
    public static function Check(array $data, array $rule, array $message, int $Code): void
    {
        $check = Validation::check($data, $rule, $message);
        if ($check->isFail()) {
            throw new Exception($check->firstError(), $Code);
        }

    }
}