<?php

namespace Coral;

use Exception;

class CoralException extends Exception
{
    const System_ERROR_CODE          = 33;  // 系统错误
    const Request_Confine_ERROR_CODE = 44;  // 系统限制
    const Request_Xxs_ERROR_CODE     = 55;  // XXS请求非法
    const Router_ERROR_CODE          = 66;  // 路由方法不存在
    const EasyPdo_ERROR_CODE         = 111; // 数据库PDO链接错误
    const EasyRedis_ERROR_CODE       = 222; // redis链接错误
    const EasyMQ_ERROR_CODE          = 333; // MQ链接错误
    const Logger_ERROR_CODE          = 444; // 日志错误
    const UtilTool_ERROR_CODE        = 555; // 工具集错误
    const EastXls_ERROR_CODE         = 666; // Excel操作错误
    const Pdo_Util_ERROR_CODE        = 777; // PdoUtil操作错误
    const Pdo_Secure_ERROR_CODE      = 999; // 系统安全性告警
}