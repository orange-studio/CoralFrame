<?php

namespace Coral;

use Coral\Tool\FileDirTool;
use Coral\Tool\UtilTool;

class Install
{
    public function InitDir()
    {
        $source = UtilTool::NormalizedPath(CORAL_COMPOSER_ROOT . DIRECTORY_SEPARATOR . 'template');
        $target = CORAL_ROOT;
        FileDirTool::copyFromName($source, $target, '.example', true);
        $composerJsonFilePath = CORAL_ROOT . DIRECTORY_SEPARATOR . 'composer.json';
        $composerJson         = file_get_contents($composerJsonFilePath);
        if (empty($composerJson)) {
            exit('composer.json 文件缺失');
        }
        $composerArr = json_decode($composerJson, true);
        if (empty($composerArr['autoload']['psr-4'])) {
            $composerArr['autoload']['psr-4'] = [
                'App\\'     => 'App/',
                'Process\\' => 'Process/'
            ];
        } else {
            $composerArr['autoload']['psr-4']['App\\']     = 'App/';
            $composerArr['autoload']['psr-4']['Process\\'] = 'Process/';
        }
        file_put_contents($composerJsonFilePath, json_encode($composerArr,JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
    }
}