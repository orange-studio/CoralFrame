<?php

namespace Coral\Instance;

use Coral\CoralException;
use Coral\Extend\DB;
use Throwable;

class DBInstance
{
    protected static ?DB $_instance = null;

    /**
     * @throws CoralException
     */
    public static function getInstance(string $configName = 'default'): DB
    {
        try {
            if (self::$_instance === null) {
                self::$_instance = new DB($configName);
            }
            return self::$_instance;
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), $th->getCode());
        }
    }
}