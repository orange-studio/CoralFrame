<?php

namespace Coral\Instance;

use Coral\Core\Route\RouteCollector;

class RouterInstance
{
    protected static ?RouteCollector $_instance = null;

    public static function getInstance(): RouteCollector
    {
        if (self::$_instance === null) {
            self::$_instance = new RouteCollector();
        }

        return self::$_instance;
    }
}