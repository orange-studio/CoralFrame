<?php

namespace Coral\Instance;

use Coral\CoralException;
use Coral\Extend\MQ;
use Throwable;

class MQInstance
{
    protected static ?MQ $_instance = null;

    /**
     * @throws CoralException
     */
    public static function getInstance(string $configName = 'default'): MQ
    {
        try {
            if (self::$_instance === null) {
                self::$_instance = new MQ($configName);
            }
            return self::$_instance;
        } catch (Throwable $th) {
            throw new CoralException($th->getMessage(), $th->getCode());
        }
    }
}