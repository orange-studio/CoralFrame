# Coral

### 介绍

| 文件目录          | 描述             |
|:--------------|:---------------|
| `main.php`    | 全局入口文件         |
| `.env`        | 环境配置           |
| `config.yaml` | 系统配置文件         |
| `App`         | 应用业务代码         |
| `Process`     | 系统定时任务以及进程业务代码 |

#### 安装框架

```shell
php vendor/bin/CoralInstall.php
composer dump-autoload
```

#### 启动

```shell
php vendor/bin/Coral.php start
```

#### 停止

```shell
php vendor/bin/Coral.php stop
```

#### 重启

```shell
php vendor/bin/Coral.php restart
```

#### 查看运行状态

```shell
php vendor/bin/Coral.php status
```